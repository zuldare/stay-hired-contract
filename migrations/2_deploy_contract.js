const SHDCrowdsale = artifacts.require("./contracts/SHDCrowdsale.sol")

module.exports = deployer => {
    const startTime = web3.eth.getBlock(web3.eth.blockNumber).timestamp + 5 * 60 // 5 minutes in da future
    const endTime = startTime + (86400 * 365) // give or take a year
    const rate = new web3.BigNumber(200)
    const wallet = web3.eth.accounts[0]

    console.log(new Date(startTime * 1000), new Date(endTime * 1000))

    deployer.deploy(SHDCrowdsale, startTime, endTime, rate, wallet, {
        // gas: 4700000,
        // from: web3.eth.accounts[0]
    })
}