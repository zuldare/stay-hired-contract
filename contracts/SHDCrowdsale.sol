pragma solidity ^0.4.17;

import './SHDCoin.sol';
import 'zeppelin-solidity/contracts/crowdsale/Crowdsale.sol';

contract SHDCrowdsale is Crowdsale {

    function SHDCrowdsale(uint256 _startTime, uint256 _endTime, uint256 _rate, address _wallet)
        Crowdsale(_startTime, _endTime, _rate, _wallet) 
        {
        }

    function createTokenContract() internal returns (MintableToken) {
        return new SHDCoin();
    }
}