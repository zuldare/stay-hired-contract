pragma solidity ^0.4.17;

contract Storage {
   uint value;

   function setValue( uint _value ) public { value = _value; } 

   function getValue() public constant returns ( uint ) {
       return value;
   }

   function () public payable {
       setValue(msg.value);
   }
}