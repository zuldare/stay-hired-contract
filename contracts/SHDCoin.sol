pragma solidity ^0.4.17;

import 'zeppelin-solidity/contracts/token/MintableToken.sol';

contract SHDCoin is MintableToken {
    string public name = "Gone Crazy Token";
    string public symbol = "WGC";
    uint8 public decimals = 18;
}