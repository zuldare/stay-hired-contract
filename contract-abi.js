const fs = require('fs')

const storeAbi = name => {
    const data = fs.readFileSync(`./build/contracts/${ name }.json`)
    fs.writeFileSync(`abis/${name}.js`, `var ${name} = ${data}`)
}

// console.log(getAbi('Storage').abi)

storeAbi('SHDCoin')
storeAbi('SHDCrowdsale')